defmodule TransactionChecker.CLI do

  def main(args) do
    args 
    |> parse_args 
    |> process 
    |> Enum.each(fn(line) -> IO.puts(line) end)
  end

  def process([]) do
    IO.puts "No arguments given"
  end

  def process(options) do
    path = options[:path]

    result = path
             |> File.ls!()
             |> Enum.filter(fn(name) -> String.ends_with?(name, "txt") end)
             |> Enum.reduce([], fn(filename, acc) -> acc ++ [File.stream!("#{path}#{filename}")] end)
             |> Stream.concat
             |> Enum.map(&String.trim/1)
             |> Enum.filter(fn(line) -> String.contains?(line, ".zip") end)
             |> Enum.map(fn(line) -> remove_zip_prefix(line) end)
             |> Enum.filter(fn(id) -> exists_on_es(id) == 0 end)

    result
  end

  defp parse_args(args) do
    {options, _, _} = OptionParser.parse(args,
      switches: [path: :string]
    )
    options
  end

  defp remove_zip_prefix(string) do
    result = string
             |> String.split(".zip")
             |> Enum.at(0)

    result
  end

  defp exists_on_es(id) do

    # Do the real shit here
    result = Elastix.Search.count("http://localhost:39200", "archive_*2019", ["doc"], %{query: %{term: %{"transactionId.keyword": id}}})
             |> elem(1)
             |> Map.get(:body)
             |> get_in(["count"])

    result
  end

end
